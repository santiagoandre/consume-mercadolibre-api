import json
import requests


api_url_base = 'https://api.mercadolibre.com/'
'''
headers = {'Content-Type': 'application/json',
           'User-Agent': 'Python Student',
           'Accept': 'application/vnd.github.v3+json'}
'''

category_id = "MLA1055"
endpoint = "{}sites/MLA/search?category={}".format(api_url_base,category_id)

response = requests.get(endpoint)# , headers=headers)

if response.status_code == 200:
	print(response.content)
else:
	print('[!] HTTP {0} calling [{1}]'.format(response.status_code, api_url))
        